yamlloader plugin for `Tutor <https://docs.tutor.overhang.io>`__
===================================================================================

Installation
------------

::

    pip install git+https://github.com/myusername/tutor-contrib-yamlloader

Usage
-----

::

    tutor plugins enable yamlloader


License
-------

This software is licensed under the terms of the AGPLv3.